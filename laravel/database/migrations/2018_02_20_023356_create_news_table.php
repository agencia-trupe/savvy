<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('data');
            $table->string('titulo');
            $table->string('capa');
            $table->text('descricao');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('news');
    }
}
