<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBotanicalsItemsTable extends Migration
{
    public function up()
    {
        Schema::create('botanicals_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->string('titulo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('botanicals_items');
    }
}
