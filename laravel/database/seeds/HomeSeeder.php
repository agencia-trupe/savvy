<?php

use Illuminate\Database\Seeder;

class HomeSeeder extends Seeder
{
    public function run()
    {
        DB::table('home')->insert([
            'video' => '',
            'titulo' => '',
            'subtitulo' => '',
        ]);
    }
}
