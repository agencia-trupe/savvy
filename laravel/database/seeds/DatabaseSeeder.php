<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
		$this->call(CocktailsTextoSeeder::class);
		$this->call(BotanicalsSeeder::class);
		$this->call(OurTimeSeeder::class);
		$this->call(OurGinSeeder::class);
		$this->call(HomeSeeder::class);
		$this->call(ConfiguracoesSeeder::class);
        $this->call(ContatoTableSeeder::class);

        Model::reguard();
    }
}
