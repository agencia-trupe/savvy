<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Home extends Model
{
    protected $table = 'home';

    protected $guarded = ['id'];

    public static function upload_video() {
        $file = request()->file('video');

        $path = 'assets/videos/';
        $name = str_slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)).'_'.date('YmdHis').'.'.$file->getClientOriginalExtension();

        $file->move($path, $name);

        return $name;
    }
}
