<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class OurGin extends Model
{
    protected $table = 'our_gin';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 925,
            'height' => null,
            'path'   => 'assets/img/our-gin/'
        ]);
    }

}
