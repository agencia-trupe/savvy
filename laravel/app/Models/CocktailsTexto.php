<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class CocktailsTexto extends Model
{
    protected $table = 'cocktails_texto';

    protected $guarded = ['id'];

}
