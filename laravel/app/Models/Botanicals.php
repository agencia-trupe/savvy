<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Botanicals extends Model
{
    protected $table = 'botanicals';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 925,
            'height' => null,
            'path'   => 'assets/img/botanicals/'
        ]);
    }

}
