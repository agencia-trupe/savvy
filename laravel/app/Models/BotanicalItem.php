<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class BotanicalItem extends Model
{
    protected $table = 'botanicals_items';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 75,
            'height' => 75,
            'path'   => 'assets/img/botanicals-items/'
        ]);
    }
}
