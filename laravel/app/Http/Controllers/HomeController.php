<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Carbon\Carbon;

use App\Models\Banner;
use App\Models\OurGin;
use App\Models\OurTime;
use App\Models\Botanicals;
use App\Models\BotanicalItem;
use App\Models\CocktailsTexto;
use App\Models\Cocktail;
use App\Models\News;

class HomeController extends Controller
{
    public function index()
    {
        $banners = Banner::ordenados()->get();

        return view('frontend.home', compact('banners'));
    }

    public function ourGin()
    {
        return view('frontend.our-gin')->with(['ourGin' => OurGin::first()]);
    }

    public function ourTime()
    {
        return view('frontend.our-time')->with(['ourTime' => OurTime::first()]);
    }

    public function botanicals()
    {
        return view('frontend.botanicals')->with([
            'botanicals' => Botanicals::first(),
            'items'      => BotanicalItem::ordenados()->get()
        ]);
    }

    public function cocktails()
    {
        return view('frontend.cocktails')->with([
            'cocktailsTexto' => CocktailsTexto::first(),
            'cocktails'      => Cocktail::ordenados()->get()
        ]);
    }

    public function newsEvents($ano = null, $mes = null)
    {
        $datas = array_map(function($data) {
            return [
                'ano' => explode('/', $data)[2],
                'mes' => explode('/', $data)[1]
            ];
        }, News::orderBy('data', 'DESC')->pluck('data')->toArray());

        $datas = array_unique($datas, SORT_REGULAR);

        if ($ano && $mes) {
            if (!in_array(['ano' => $ano, 'mes' => $mes], $datas)) {
                return redirect()->route('news-events');
            }

            return view('frontend.news')->with([
                'news'  => News::orderBy('data', 'DESC')
                                ->orderBy('id', 'DESC')
                                ->whereMonth('data', '=', $mes)
                                ->whereYear('data', '=', $ano)
                                ->paginate(10),
                'datas' => $datas,
                'ano'   => $ano,
                'mes'   => $mes
            ]);
        }

        if ($ano || $mes) {
            return redirect()->route('news-events');
        }

        return view('frontend.news')->with([
            'news'  => News::orderBy('data', 'DESC')->orderBy('id', 'DESC')->paginate(10),
            'datas' => $datas
        ]);
    }
}
