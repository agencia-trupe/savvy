<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\BotanicalsItemsRequest;
use App\Http\Controllers\Controller;

use App\Models\BotanicalItem;

class BotanicalsItemsController extends Controller
{
    public function index()
    {
        $registros = BotanicalItem::ordenados()->get();

        return view('painel.botanicals-items.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.botanicals-items.create');
    }

    public function store(BotanicalsItemsRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = BotanicalItem::upload_imagem();

            BotanicalItem::create($input);

            return redirect()->route('painel.botanicals-items.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(BotanicalItem $registro)
    {
        return view('painel.botanicals-items.edit', compact('registro'));
    }

    public function update(BotanicalsItemsRequest $request, BotanicalItem $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = BotanicalItem::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.botanicals-items.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(BotanicalItem $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.botanicals-items.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
