<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\NewsRequest;
use App\Http\Controllers\Controller;

use App\Models\News;

class NewsController extends Controller
{
    public function index()
    {
        $registros = News::orderBy('data', 'DESC')->orderBy('id', 'DESC')->paginate(10);

        return view('painel.news.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.news.create');
    }

    public function store(NewsRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = News::upload_capa();

            News::create($input);

            return redirect()->route('painel.news.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(News $registro)
    {
        return view('painel.news.edit', compact('registro'));
    }

    public function update(NewsRequest $request, News $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = News::upload_capa();

            $registro->update($input);

            return redirect()->route('painel.news.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(News $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.news.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
