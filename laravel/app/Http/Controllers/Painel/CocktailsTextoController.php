<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CocktailsTextoRequest;
use App\Http\Controllers\Controller;

use App\Models\CocktailsTexto;

class CocktailsTextoController extends Controller
{
    public function index()
    {
        $registro = CocktailsTexto::first();

        return view('painel.cocktails-texto.edit', compact('registro'));
    }

    public function update(CocktailsTextoRequest $request, CocktailsTexto $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.cocktails-texto.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
