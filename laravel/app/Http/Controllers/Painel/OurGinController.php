<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\OurGinRequest;
use App\Http\Controllers\Controller;

use App\Models\OurGin;

class OurGinController extends Controller
{
    public function index()
    {
        $registro = OurGin::first();

        return view('painel.our-gin.edit', compact('registro'));
    }

    public function update(OurGinRequest $request, OurGin $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = OurGin::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.our-gin.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
