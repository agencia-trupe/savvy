<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\OurTimeRequest;
use App\Http\Controllers\Controller;

use App\Models\OurTime;

class OurTimeController extends Controller
{
    public function index()
    {
        $registro = OurTime::first();

        return view('painel.our-time.edit', compact('registro'));
    }

    public function update(OurTimeRequest $request, OurTime $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = OurTime::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.our-time.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
