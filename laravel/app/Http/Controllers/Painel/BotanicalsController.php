<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\BotanicalsRequest;
use App\Http\Controllers\Controller;

use App\Models\Botanicals;

class BotanicalsController extends Controller
{
    public function index()
    {
        $registro = Botanicals::first();

        return view('painel.botanicals.edit', compact('registro'));
    }

    public function update(BotanicalsRequest $request, Botanicals $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Botanicals::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.botanicals.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
