<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CocktailsRequest;
use App\Http\Controllers\Controller;

use App\Models\Cocktail;

class CocktailsController extends Controller
{
    public function index()
    {
        $registros = Cocktail::ordenados()->get();

        return view('painel.cocktails.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.cocktails.create');
    }

    public function store(CocktailsRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Cocktail::upload_imagem();

            Cocktail::create($input);

            return redirect()->route('painel.cocktails.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Cocktail $registro)
    {
        return view('painel.cocktails.edit', compact('registro'));
    }

    public function update(CocktailsRequest $request, Cocktail $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Cocktail::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.cocktails.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Cocktail $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.cocktails.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
