<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('our-gin', 'HomeController@ourGin')->name('our-gin');
    Route::get('our-time', 'HomeController@ourTime')->name('our-time');
    Route::get('botanicals', 'HomeController@botanicals')->name('botanicals');
    Route::get('cocktails', 'HomeController@cocktails')->name('cocktails');
    Route::get('news-events/{ano?}/{mes?}', 'HomeController@newsEvents')->name('news-events');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('banners', 'BannersController');
		Route::resource('cocktails-texto', 'CocktailsTextoController', ['only' => ['index', 'update']]);
		Route::resource('cocktails', 'CocktailsController');
		Route::resource('news', 'NewsController');
		Route::resource('botanicals-items', 'BotanicalsItemsController');
		Route::resource('botanicals', 'BotanicalsController', ['only' => ['index', 'update']]);
		Route::resource('our-time', 'OurTimeController', ['only' => ['index', 'update']]);
		Route::resource('our-gin', 'OurGinController', ['only' => ['index', 'update']]);
		// Route::resource('home', 'HomeController', ['only' => ['index', 'update']]);
		Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);

        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
