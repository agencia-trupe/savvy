<?php

namespace App\Helpers;

class Tools
{

    public static function loadJquery()
    {
        return '<script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>'."\n\t".'<script>window.jQuery || document.write(\'<script src="' . asset("assets/vendor/jquery/dist/jquery.min.js") . '"><\/script>\')</script>';
    }

    public static function loadJs($path)
    {
        return '<script src="' . asset('assets/'.$path) .'"></script>';
    }

    public static function loadCss($path)
    {
        return '<link rel="stylesheet" href="' . asset('assets/'.$path) . '">';
    }

    public static function isActive($routeName)
    {
        return str_is($routeName, \Route::currentRouteName());
    }

    public static function formataData($data)
    {
        $meses = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];

        list($dia, $mes, $ano) = explode('/', $data);

        return $meses[(int) $mes - 1] . ' | ' . $dia . ' | ' . $ano;
    }

    public static function formataMesAno($mes, $ano)
    {
        $meses = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];

        return $meses[(int) $mes - 1] . ' | ' . $ano;
    }

}
