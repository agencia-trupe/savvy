@extends('frontend.common.template')

@section('content')

    <div class="padrao">
        <img src="{{ asset('assets/img/our-gin/'.$ourGin->imagem) }}" class="padrao-imagem" alt="">
        <div class="padrao-texto">
            {!! $ourGin->texto !!}
        </div>
    </div>

@endsection
