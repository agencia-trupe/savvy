@extends('frontend.common.template')

@section('content')

    <div class="padrao">
        <img src="{{ asset('assets/img/our-time/'.$ourTime->imagem) }}" class="padrao-imagem" alt="">
        <div class="padrao-texto">
            {!! $ourTime->texto !!}
        </div>
    </div>

@endsection
