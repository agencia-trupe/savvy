@extends('frontend.common.template')

@section('content')

    <div class="banners">
        @foreach($banners as $banner)
        <img src="{{ asset('assets/img/banners/'.$banner->imagem) }}" alt="">
        @endforeach
    </div>

@endsection
