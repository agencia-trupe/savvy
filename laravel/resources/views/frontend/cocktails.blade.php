@extends('frontend.common.template')

@section('content')

    <div class="padrao">
        <div class="padrao-texto">
            <p>{!! $cocktailsTexto->texto !!}</p>

            <div class="cocktails">
                @foreach($cocktails as $cocktail)
                <a href="#" class="cocktail-handle">{{ $cocktail->titulo }}</a>
                <div class="cocktail-conteudo">
                    <img src="{{ asset('assets/img/cocktails/'.$cocktail->imagem) }}" alt="">
                    <div class="texto">
                        <h3>{{ $cocktail->titulo }}</h3>
                        <p>{!! $cocktail->ingredientes !!}</p>
                        <p>{!! $cocktail->preparo !!}</p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
