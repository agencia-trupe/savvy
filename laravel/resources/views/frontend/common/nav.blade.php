<a href="{{ route('our-gin') }}" @if(Tools::isActive('our-gin')) class="active" @endif>
    OUR GIN
</a>
<a href="{{ route('our-time') }}" @if(Tools::isActive('our-time')) class="active" @endif>
    OUR TIME
</a>
<a href="{{ route('botanicals') }}" @if(Tools::isActive('botanicals')) class="active" @endif>
    BOTANICALS
</a>
<a href="{{ route('cocktails') }}" @if(Tools::isActive('cocktails')) class="active" @endif>
    COCKTAILS
</a>
<a href="{{ route('news-events') }}" @if(Tools::isActive('news-events')) class="active" @endif>
    NEWS&EVENTS
</a>
<a href="mailto:{{ $contato->email }}">GET IN TOUCH</a>
{{--<a href="#">SHOP</a>--}}
