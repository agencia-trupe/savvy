    @if($contato->instagram || $contato->facebook)
    <div class="social-fixed">
        @foreach(['instagram', 'facebook'] as $s)
            @if($contato->{$s})
            <a href="{{ $contato->{$s} }}" class="{{ $s }}" target="_blank">{{ $s }}</a>
            @endif
        @endforeach
    </div>
    @endif

    <header>
        <nav id="nav-mobile">
            @include('frontend.common.nav')
        </nav>

        <a href="{{ route('home') }}" class="logo">{{ $config->nome_do_site }}</a>

        <nav id="nav-desktop">
            @include('frontend.common.nav')
        </nav>

        <button id="mobile-toggle" type="button" role="button">
            <span class="lines"></span>
        </button>
    </header>
