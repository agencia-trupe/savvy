@extends('frontend.common.template')

@section('content')

    <div class="padrao">
        <img src="{{ asset('assets/img/botanicals/'.$botanicals->imagem) }}" class="padrao-imagem" alt="">
        <div class="padrao-texto">
            {!! $botanicals->texto !!}

            <div class="botanicals-items">
                <h3>THE {{ count($items) }} BOTANICALS</h3>
                <div>
                    @foreach($items as $item)
                    <div class="item">
                        <img src="{{ asset('assets/img/botanicals-items/'.$item->imagem) }}" alt="">
                        <span>{{ $item->titulo }}</span>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
