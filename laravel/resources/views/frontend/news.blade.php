@extends('frontend.common.template')

@section('content')

    <div class="news">
        <main>
            @foreach($news as $n)
            <article>
                <img src="{{ asset('assets/img/news/'.$n->capa) }}" alt="">
                <span class="data">{{ Tools::formataData($n->data) }}</span>
                <h2>{{ $n->titulo }}</h2>
                {!! $n->descricao !!}
            </article>
            @endforeach

            {!! $news->render() !!}
        </main>

        <aside>
            @foreach($datas as $data)
            <a href="{{ route('news-events', [$data['ano'], $data['mes']]) }}" @if(isset($ano) && isset($mes) && $ano == $data['ano'] && $mes == $data['mes']) class="active" @endif>
                {{ Tools::formataMesAno($data['mes'], $data['ano']) }}
            </a>
            @endforeach
        </aside>
    </div>

@endsection
