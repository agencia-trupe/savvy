@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            Botanicals
            <a href="{{ route('painel.botanicals-items.index') }}" class="btn btn-info btn-sm pull-right"><span class="glyphicon glyphicon-list" style="margin-right:10px;"></span>Editar Items</a>
        </h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.botanicals.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.botanicals.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
