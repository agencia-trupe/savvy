@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Our Time</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.our-time.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.our-time.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
