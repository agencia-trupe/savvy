@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Botanicals Items /</small> Adicionar Botanical Item</h2>
    </legend>

    {!! Form::open(['route' => 'painel.botanicals-items.store', 'files' => true]) !!}

        @include('painel.botanicals-items.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
