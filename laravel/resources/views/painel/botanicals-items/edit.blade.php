@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Botanicals Items /</small> Editar Botanical Item</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.botanicals-items.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.botanicals-items.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
