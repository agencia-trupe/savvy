@extends('painel.common.template')

@section('content')

    <a href="{{ route('painel.cocktails.index') }}" title="Voltar para Cocktails" class="btn btn-sm btn-default">
        &larr; Voltar para Cocktails
    </a>

    <legend>
        <h2>Cocktails Texto</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.cocktails-texto.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.cocktails-texto.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
