<ul class="nav navbar-nav">
	<li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.banners.index') }}">Banners</a>
	</li>
    {{--<li @if(str_is('painel.home*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.home.index') }}">Home</a>
    </li>--}}
    <li @if(str_is('painel.our-gin*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.our-gin.index') }}">Our Gin</a>
    </li>
    <li @if(str_is('painel.our-time*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.our-time.index') }}">Our Time</a>
    </li>
    <li @if(str_is('painel.botanicals*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.botanicals.index') }}">Botanicals</a>
    </li>
	<li @if(str_is('painel.cocktails*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.cocktails.index') }}">Cocktails</a>
	</li>
    <li @if(str_is('painel.news*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.news.index') }}">News</a>
    </li>
	<li @if(str_is('painel.contato*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.contato.index') }}">Contato</a>
    </li>
    {{--
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>
    --}}
</ul>
