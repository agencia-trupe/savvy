@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Our Gin</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.our-gin.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.our-gin.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
