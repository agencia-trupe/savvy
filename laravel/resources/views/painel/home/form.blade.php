@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('video', 'Vídeo') !!}
    @if(isset($registro) && $registro->video)
    <p>
        <a href="{{ url('assets/videos/'.$registro->video) }}" target="_blank" style="display:block;">{{ $registro->video }}</a>
    </p>
    @endif
    {!! Form::file('video', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('subtitulo', 'Subtítulo') !!}
    {!! Form::text('subtitulo', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
