@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Cocktails /</small> Adicionar Cocktail</h2>
    </legend>

    {!! Form::open(['route' => 'painel.cocktails.store', 'files' => true]) !!}

        @include('painel.cocktails.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
