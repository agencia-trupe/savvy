@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Cocktails /</small> Editar Cocktail</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.cocktails.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.cocktails.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
