@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>News /</small> Editar News</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.news.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.news.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
