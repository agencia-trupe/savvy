import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

$('.banners').cycle();

$('.cocktail-handle').click(function(event) {
    event.preventDefault();

    $(this).toggleClass('open').next().slideToggle();
});
